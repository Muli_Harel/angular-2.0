import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; } 
  `]
})
export class UsersComponent implements OnInit {

 users = [
    {name:'Muli', email:'Muli@mail.com'},
    {name:'Kety', email:'Kety@mail.com'},
    {name:'Harel', email:'Harel@mail.com'}
  ]
  constructor() { }

  ngOnInit() {
  }

} 