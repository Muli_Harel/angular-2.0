import { Component, OnInit } from '@angular/core';
import {Post} from './post';
@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

  post:Post;

  constructor() { }

  ngOnInit() {
  }

}