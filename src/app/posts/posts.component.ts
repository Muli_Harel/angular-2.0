import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';

@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts;

  currentPost;

  constructor(private _postsService: PostsService) {
    this.posts = this._postsService.getPosts();
   }

  ngOnInit() {
  }

}