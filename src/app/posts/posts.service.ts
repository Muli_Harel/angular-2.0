import { Injectable } from '@angular/core';

@Injectable()
export class PostsService {

    posts = [
    {title:'Title 1',content:'Content 1',author:'Author 1'},
    {title:'Title 2',content:'Content 2',author:'Author 2'},
    {title:'Title 3',content:'Content 3',author:'Author 3'},
    {title:'Title 4',content:'Content 4',author:'Author 4'}
  ]
  getPosts(){
		return this.posts;
	}
  constructor() { }

}